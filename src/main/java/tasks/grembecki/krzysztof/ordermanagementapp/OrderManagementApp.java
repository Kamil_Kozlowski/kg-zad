package tasks.grembecki.krzysztof.ordermanagementapp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@EnableAutoConfiguration
//@ComponentScan(basePackages = {"tasks.grembecki.krzysztof.ordermanagementapp"})
//@EnableJpaRepositories(basePackages = "tasks.grembecki.krzysztof.ordermanagementapp.repository")
//@EnableTransactionManagement
//@EntityScan(basePackages = "tasks.grembecki.krzysztof.ordermanagementapp.entity")

@SpringBootApplication
public class OrderManagementApp extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OrderManagementApp.class);
	}

	private  final  static Logger log = LoggerFactory.getLogger(OrderManagementApp.class);
	public static void main(String[] args) {
		SpringApplication.run(OrderManagementApp.class, args);
	}


}
