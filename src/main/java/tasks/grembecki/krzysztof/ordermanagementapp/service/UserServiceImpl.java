package tasks.grembecki.krzysztof.ordermanagementapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.User;
import tasks.grembecki.krzysztof.ordermanagementapp.repository.RoleRepository;
import tasks.grembecki.krzysztof.ordermanagementapp.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole("USER");
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public String getUserIdByUserName(String userName) {
        return this.userRepository.findUserIdByUserName(userName);
    }

    public List<User> listAll(){
        return userRepository.findAll();
    }

    public User get(String id) {
        return  userRepository.findById(id).get();
    }

    public void delete(String id) {
        userRepository.deleteById(id);
    }
}
