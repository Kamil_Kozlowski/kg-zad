package tasks.grembecki.krzysztof.ordermanagementapp.service;

import tasks.grembecki.krzysztof.ordermanagementapp.entity.Order;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Product;

import java.util.List;

public interface OrderService {


    public List<Order> listAll();
    public List<Order> getAllByUserId(String userId);
    public void save(Order order);
    public Order get(String id);
    public void delete(String id);
    public void addProduct(Product product);
    public void removeProduct(Product product);
    public List<Product> addToOrder();
    public List<Product> getProductsList();
}
