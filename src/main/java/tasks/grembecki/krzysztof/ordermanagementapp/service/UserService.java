package tasks.grembecki.krzysztof.ordermanagementapp.service;

import tasks.grembecki.krzysztof.ordermanagementapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> listAll();

    void save(User user);

    User get(String id);

    void delete(String id);

    Optional<User> findByUsername(String username);

    String getUserIdByUserName(String userName);
}
