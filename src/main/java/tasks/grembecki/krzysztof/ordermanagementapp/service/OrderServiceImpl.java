package tasks.grembecki.krzysztof.ordermanagementapp.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Order;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Product;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.User;
import tasks.grembecki.krzysztof.ordermanagementapp.repository.OrderRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Getter
    @Setter
    private List<Product> productsList = new ArrayList<>();

    @Autowired
    private UserService userService;

    @Autowired
    private OrderRepository orderRepository;

    public List<Order> listAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getAllByUserId(String userId) {
        return this.orderRepository.findAllByUserId(userId);
    }


    public void save(Order order) {
        orderRepository.save(order);
    }
    public Order get(String id) {
        return orderRepository.findById(id).get();
    }

    public void delete(String id) {
        orderRepository.deleteById(id);
    }

    public void addProduct(Product product) {
        productsList.add(product);
    }

    public void removeProduct(Product product) {
        productsList.remove(product);
    }

    public List<Product> addToOrder() {
        return productsList;
    }
}

