package tasks.grembecki.krzysztof.ordermanagementapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    private String name;

//    @ManyToMany(mappedBy = "roles")
//    private Set<User> users;

}
