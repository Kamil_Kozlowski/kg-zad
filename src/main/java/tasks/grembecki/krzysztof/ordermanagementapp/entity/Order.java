package tasks.grembecki.krzysztof.ordermanagementapp.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @Column(name = "order_id", length = 40, nullable = false)
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    @ElementCollection
    @Column(name = "list_of_products_id", nullable = false)
    private List<Product> productsList = new ArrayList<>();

    @Column(name = "creation_date", nullable = false)
    private Date creationDate;


//        @Column(name = "user_id", unique = true, nullable = false)
    @ManyToOne
    private User user;
}
