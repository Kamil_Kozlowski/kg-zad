package tasks.grembecki.krzysztof.ordermanagementapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Product;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long > {
    Optional<Product> findByName(@Param("name") String name);
    Optional<Product> findTopByOrderByIdDesc();
}
