package tasks.grembecki.krzysztof.ordermanagementapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Order;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, String > {
    Optional<Order> findById(String id);
    Optional<List<Long>> findAllByCreationDate(Date date);
    Order findByUser(User user);

    @Query("select o from Order o where o.user.id = ?1")
    List<Order> findAllByUserId(String userId);
//    Optional<List<Order>> findAllByUser_idMatches(String id); //doesn't work

}
