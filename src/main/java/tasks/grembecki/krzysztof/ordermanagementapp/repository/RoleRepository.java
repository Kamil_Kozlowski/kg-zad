package tasks.grembecki.krzysztof.ordermanagementapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
