package tasks.grembecki.krzysztof.ordermanagementapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Order;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String > {
    Optional<User> findById(String id);
    Optional<User> findByUsername(String username);

    @Query("select id from User u where u.username = ?1")
    String findUserIdByUserName(String username);
}
