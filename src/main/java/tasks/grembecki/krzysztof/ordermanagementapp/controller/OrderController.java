package tasks.grembecki.krzysztof.ordermanagementapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Product;
import tasks.grembecki.krzysztof.ordermanagementapp.service.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class OrderController {

    private final OrderService orderService;

    private final ProductService productService;

    private final UserService userService;

    @Autowired
    public OrderController(OrderService orderService, ProductService productService, UserService userService) {
        this.orderService = orderService;
        this.productService = productService;
        this.userService = userService;
    }

    @GetMapping("/order-list")
    public String showOrderList(Model model, Principal principal) {
        model.addAttribute("orders", orderService.getAllByUserId(this.userService.getUserIdByUserName(principal.getName())));
        return "order-list";
    }

    @GetMapping("/order-add")
    public String showOrderAdd(Product product) {
        return "order-add";
    }

    @PostMapping("/order-add-product")
    public String addProductToOrder(@Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "order-list";
        }
//        orderService.addProduct(product);
        orderService.getProductsList().add(product);
//        orderService.save(order);
        model.addAttribute("products", orderService.listAll());
        return "order-list";
    }



}
