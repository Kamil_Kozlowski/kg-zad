package tasks.grembecki.krzysztof.ordermanagementapp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import tasks.grembecki.krzysztof.ordermanagementapp.entity.Product;
import tasks.grembecki.krzysztof.ordermanagementapp.repository.OrderRepository;
import tasks.grembecki.krzysztof.ordermanagementapp.repository.ProductRepository;
import tasks.grembecki.krzysztof.ordermanagementapp.service.ProductService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ProductController {

    private final ProductService productService;

    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ProductService productService, ProductRepository productRepository){
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @GetMapping(value = {"/", "/index"})
    public String index(ModelMap modelMap) {
        modelMap.put("products", productService.listAll());
        return "product-list";
    }

    @GetMapping("/product-add")
    public String showSignUpForm(Product product) {
        return "product-add";
    }

    @GetMapping(value = "/product-list")
    public String showAll(Model model) {
        model.addAttribute("products", productService.listAll());

        return "product-list";
    }

    @PostMapping("/product-add")
    public String addProduct(@Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "product-add";
        }
        productService.save(product);
        model.addAttribute("products", productService.listAll());
        return "product-list";
    }

//    @GetMapping("product/edit/{id}")
//    public String showUpdateForm(@PathVariable("id") long id,@Valid Product product, Model model) {
////        product = productService.get(id);
//        model.addAttribute("products", product);
//        return "product-update";
//    }
//
//    @PostMapping("product/update/{id}")
//    public String updateProduct(@PathVariable("id") long id, @Valid Product product, BindingResult result, Model model) {
//        if (result.hasErrors()) {
//            product.setId(id);
//            return "product-update";
//        }
//
//        productService.save(product);
//        model.addAttribute("products", productService.listAll());
//        return "product-list";
//    }

//    @GetMapping("product/delete/{id}")
//    public String deleteProduct(@PathVariable("id") long id, Model model) {
////        Product product = productService.get(id);
//        productService.delete(id);
//        model.addAttribute("products", productService.listAll());
//        return "product-list";
//    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id,@Valid Product product, Model model) {
        System.out.println("weszlo?");
        product = productRepository.findById(product.getId()).orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
        model.addAttribute("products", product);
        return "product-update";
    }

    @PostMapping("/update/{id}")
    public String updateProduct(@PathVariable("id") long id, @Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            product.setId(id);
            return "product-update";
        }

        productRepository.save(product);
        model.addAttribute("products", productRepository.findAll());
        return "product-list";
    }

    @GetMapping("/delete/{id}")
    public String deleteProduct(@PathVariable("id") long id, Model model) {
        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        productRepository.delete(product);
        model.addAttribute("products", productRepository.findAll());
        return "product-list";
    }
}
