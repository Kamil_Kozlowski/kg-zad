INSERT INTO USERS (user_id,user_name, password)
VALUES
-- user credentials: user/user admin/admin
('1','admin','$2a$10$WLNhybiGucP4LlaFH9L13u696DclHzqM8953JrwGaoDLYHgJkpRcK'),
('2','user','$2a$10$dJVGE88Y1dMq/zUEYgrTYeSD9UhAlb8b/sBhvJmZqgJfvxd6uxi2u');



INSERT INTO PRODUCTS (product_id, name)
VALUES
(1, 'chleb'),
(2, 'cukier'),
(3, 'maka'),
(4, 'maslo'),
(5, 'mieso'),
(6, 'ziemniaki'),
(7, 'woda'),
(8, 'sol'),
(9, 'bulka'),
(10, 'produkt1'),
(11, 'produkt2'),
(12, 'produkt3'),
(13, 'produkt4'),
(14, 'produkt5'),
(15, 'produkt6'),
(16, 'produkt7'),
(17, 'produkt8'),
(18, 'produkt9');

-- INSERT INTO ORDERS (order_id, creation_date)
-- VALUES
-- (1, '2020-01-01'),
-- (2, '2020-01-01');
--
-- INSERT INTO ORDERS_PRODUCTS_LIST (order_order_id, products_list_product_id)
-- VALUES
-- (1,1),
-- (1,2),
-- (1,3),
-- (2,8),
-- (2,5),
-- (2,6);

-- INSERT INTO ORDERS (order_id, creation_date, user_user_id)
-- VALUES
-- (1, '2020-01-01'),
-- (2, '2020-01-01');

INSERT INTO ORDERS (order_id, creation_date, user_user_id)
VALUES
(1, '2020-01-01',1),
(2, '2020-01-01',1),
(7, '2020-01-03',1),
(8, '2020-01-03',1);

INSERT INTO ORDERS (order_id, creation_date, user_user_id)
VALUES
(3, '2020-01-02',2),
(4, '2020-01-02',2),
(5, '2020-01-02',2),
(6, '2020-01-02',2);

INSERT INTO ORDERS_PRODUCTS_LIST (order_order_id, products_list_product_id)
VALUES
(1,1),
(1,2),
(1,3),
(1,11),
(1,12),
(1,13),
(1,7),
(1,8),
(1,9);

INSERT INTO ORDERS_PRODUCTS_LIST (order_order_id, products_list_product_id)
VALUES
(2,4);

INSERT INTO ORDERS_PRODUCTS_LIST (order_order_id, products_list_product_id)
VALUES
(3,6),
(4,17),
(1,18);


-- INSERT INTO ORDERS_PRODUCTS_LIST (order_order_id, products_list_product_id)
-- -- -- VALUES
-- -- -- (2,4),
-- -- -- (2,3),
-- -- -- (2,2),
-- -- -- (2,1),
-- -- -- (2,11),
-- -- -- (2,6),
-- -- -- (2,8),
-- -- -- (2,15),
-- -- -- (2,18);
